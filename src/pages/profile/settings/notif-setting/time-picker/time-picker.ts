import { Component } from "@angular/core";
import { ViewController, NavParams, ToastController, AlertController } from "ionic-angular";
// import { DatePicker } from '@ionic-native/date-picker';
import { ApiServiceProvider } from '../../../../../providers/api-service/api-service';
import { TranslateService } from "@ngx-translate/core";
// import * as moment from 'moment';

@Component({
    templateUrl: './time-picker.html',
    selector: 'app-time-picker-modal'
})


export class TimePickerModal {
    endTime: any;
    startTime: any;
    islogin: any;
    data: any;
    startDate: string;
    endDate: string;
    startT: Date;
    endT: Date;

    startHourNumber: any;
    startMinutesNumber: any;
    start_ampm: string = "AM";

    endHourNumber: any;
    endMinutesNumber: any;
    end_ampm: string = "AM"
    screenKey: string;
    schedule: any = false;
    showScheduler: boolean = false;
    showUpdateBtn: boolean = false;
    showSubmitBtn: boolean = true;
    showKey: string;
    constructor(
        navParams: NavParams,
        public apiCall: ApiServiceProvider,
        private viewCtrl: ViewController,
        private alertCtrl: AlertController,
        public translate: TranslateService,
        private toastCtrl: ToastController) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.data = navParams.get('data');
        this.screenKey = navParams.get('key');
        if (navParams.get('key') == 'tow') {
            this.showKey = this.translate.instant('Tow');
        } else {
            if (navParams.get('key') == 'parking') {
                this.showKey = this.translate.instant('Parking');
            }
        }
        let startHr, startMin, endHr, endMin;
        if (this.screenKey === 'tow') {
            if (this.data.towAlert || this.data.towTime) {
                this.schedule = this.data.towAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.towTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.towTime.start).getHours();
                startMin = new Date(this.data.towTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);

                endHr = new Date(this.data.towTime.end).getHours();
                endMin = new Date(this.data.towTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            } else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);

                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        } else if (this.screenKey === 'parking') {
            if (this.data.theftAlert || this.data.theftTime) {
                this.schedule = this.data.theftAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.theftTime) {

            }
            if (this.data.theftTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.theftTime.start).getHours();
                startMin = new Date(this.data.theftTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);

                endHr = new Date(this.data.theftTime.end).getHours();
                endMin = new Date(this.data.theftTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            } else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);

                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        }
        console.log("data parameters: ", this.data);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }

    setScheduler(sch) {
        // console.log('schedluler: ', sch)
        if (this.screenKey === 'tow') {
            this.towAlertCall(sch);
        } else if (this.screenKey === 'parking') {
            this.fonctionTest(sch);
        }
    }
    fonctionTest(d) {
        var theft;
        let that = this;
        theft = !(that.data.theftAlert);
        if (theft) {
            let alert = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to activate parking alarm? On activating this alert you will get receive notification if vehicle moves.'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: () => {
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            }
                            this.apiCall.startLoading();
                            this.apiCall.deviceupdateCall(payload)
                                .subscribe(data => {
                                    this.apiCall.stopLoading();
                                    let toast = this.toastCtrl.create({
                                        message: this.translate.instant('Parking alarm Activated!'),
                                        position: "bottom",
                                        duration: 1000
                                    });
                                    toast.present();
                                    // this.getdevices();
                                },
                                    err => {
                                        this.apiCall.stopLoading();
                                    });
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: () => {
                            that.schedule = false;
                            // that.showScheduler = false;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            })
            alert.present();
        } else {
            let alert = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to deactivate parking alarm?'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: () => {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            }
                            this.apiCall.startLoading();
                            this.apiCall.deviceupdateCall(payload)
                                .subscribe(data => {
                                    this.apiCall.stopLoading();
                                    console.log("resp: ", data)
                                    let toast = this.toastCtrl.create({
                                        message: this.translate.instant('Parking alarm Deactivated!'),
                                        position: "bottom",
                                        duration: 1000
                                    });
                                    toast.present();
                                    // this.getdevices();
                                })
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: () => {
                            that.schedule = that.data.theftAlert;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            })
            alert.present();
        }
    }
    towAlertCall(d) {
        var tow;
        let that = this;
        tow = !(that.data.towAlert);
        if (tow) {
            let alert = this.alertCtrl.create({
                title: this.translate.instant("Confirm"),
                message: this.translate.instant("Are you sure you want to activate Tow Alert alarm? On activating this alert you will get receive notification if vehicle has been towed."),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: () => {
                            // theft = !(d.theftAlert);
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            }
                            this.apiCall.startLoading();
                            this.apiCall.deviceupdateCall(payload)
                                .subscribe(data => {
                                    this.apiCall.stopLoading();
                                    console.log("resp: ", data)
                                    let toast = this.toastCtrl.create({
                                        message: "Tow Alert alarm Activated!",
                                        position: "bottom",
                                        duration: 1000
                                    });
                                    toast.present();
                                    // this.callObjFunc(d);
                                    // this.getdevices();
                                },
                                    err => {
                                        this.apiCall.stopLoading();
                                    })
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: () => {
                            that.schedule = false;
                            // that.showScheduler = false;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            })
            alert.present();
        } else {
            let alert = this.alertCtrl.create({
                title: this.translate.instant("Confirm"),
                message: this.translate.instant("Are you sure you want to deactivate Tow Alert alarm?"),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: () => {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            }
                            this.apiCall.startLoading();
                            this.apiCall.deviceupdateCall(payload)
                                .subscribe(data => {
                                    this.apiCall.stopLoading();
                                    console.log("resp: ", data)
                                    let toast = this.toastCtrl.create({
                                        message: "Tow Alert alarm Deactivated!",
                                        position: "bottom",
                                        duration: 1000
                                    });
                                    toast.present();
                                    // this.callObjFunc(d);
                                    // this.getdevices();
                                })
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: () => {
                            that.schedule = that.data.towAlert;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            })
            alert.present();
        }
    }

    checkSettings() {
        this.showScheduler = true;
    }

    reset() {
        this.startHourNumber = this.pad(1, 2);
        this.startMinutesNumber = this.pad(0, 2);

        this.endHourNumber = this.pad(1, 2);
        this.endMinutesNumber = this.pad(0, 2);
    }

    onHourClicked(key1, key) {
        // debugger
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startHourNumber) < 24) {
                    this.startHourNumber = Number(this.startHourNumber) + 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2)
                } else {
                    this.startHourNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.startHourNumber) > 1) {
                    this.startHourNumber = Number(this.startHourNumber) - 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2)
                } else {
                    this.startHourNumber = this.pad(24, 2)
                }
            }
        } else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endHourNumber) < 24) {
                    this.endHourNumber = Number(this.endHourNumber) + 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2)
                } else {
                    this.endHourNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.endHourNumber) > 1) {
                    this.endHourNumber = Number(this.endHourNumber) - 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2)
                } else {
                    this.endHourNumber = this.pad(24, 2)
                }
            }
        }

    }

    onMinuteClicked(key1, key) {
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startMinutesNumber) < 59) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) + 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2)
                } else {
                    this.startMinutesNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.startMinutesNumber) >= 1) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) - 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2)
                } else {
                    this.startMinutesNumber = this.pad(59, 2)
                }
            }
        } else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endMinutesNumber) < 59) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) + 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2)
                } else {
                    this.endMinutesNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.endMinutesNumber) >= 1) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) - 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2)
                } else {
                    this.endMinutesNumber = this.pad(59, 2)
                }
            }
        }
    }

    ampmClicked(key) {
        if (key === 'start') {
            if (this.start_ampm === "AM") {
                this.start_ampm = "PM";
            } else if (this.start_ampm === "PM") {
                this.start_ampm = "AM";
            }
        } else if (key === 'end') {
            if (this.end_ampm === "AM") {
                this.end_ampm = "PM";
            } else if (this.end_ampm === "PM") {
                this.end_ampm = "AM";
            }
        }
    }

    submit() {
        let startTime, endTime;
        var d = new Date();
        d.setHours(Number(this.startHourNumber));
        // var d = new Date();
        d.setMinutes(Number(this.startMinutesNumber));
        startTime = d;

        var d1 = new Date();
        d1.setHours(Number(this.endHourNumber));
        // var d = new Date();
        d1.setMinutes(Number(this.endMinutesNumber));
        endTime = d1;

        console.log("start time:", startTime);
        console.log("end time:", endTime);
        // console.log(d);
        let that = this;
        var payload;
        if (this.screenKey === 'parking') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "theftTime": {
                    "start": startTime,
                    "end": endTime
                }
            }
        } else if (this.screenKey === 'tow') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "towTime": {
                    "start": startTime,
                    "end": endTime
                }
            }
        }

        this.apiCall.startLoading().present();
        this.apiCall.deviceupdateCall(payload)
            .subscribe((resp) => {
                console.log('response language code ' + resp);
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant('Alert is scheduled sucessfully.'),
                    duration: 1500,
                    position: 'bottom'
                });
                toast.onDidDismiss(() => {
                    this.dismiss();
                })
                toast.present();

            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err);
                });
    }

    
}